#include <time.h>

#include <stdio.h>
#include <stdlib.h>
#include "fonctions99.h"
#include <windows.h>

/* sources

Manipulation du curseur et gestion de la couleur�: Campus ECE sur la page Projet d�informatique 1

Cours d�informatique de Mme Rendler et M. Ravaut
Cours OpenClassrooms�: https://openclassrooms.com/fr/courses/19980-apprenez-a-programmer-en-c
Forums OpenClassrooms�:

https://openclassrooms.com/forum/sujet/associer-une-lettre-a-un-chiffre-1�
https://openclassrooms.com/forum/sujet/convertir-une-lettre-en-code-ascii-99358�
Avec l'aide de M. Guillemot et de Cl�ment Dubois pour la d�finition des tuiles.
*/

void lecturescores () // appel et lecture des scores pr�c�dents
{
int a;

    FILE* fichier = NULL;
    int caracteredufichier = 0;

    fichier = fopen("scores.txt", "r");

    if (fichier != NULL)
    {

        do
        {
            caracteredufichier = fgetc(fichier);
            printf("%c", caracteredufichier);
        } while (caracteredufichier != EOF);
    } // lecture du fichier jusqu'� la fin
    printf("\ntapez 1 pour revenir en arriere \n"); // retour au menu
    do
        scanf("%d",&a);
    while (a!=1);
    if (a==1)
    {

        mode();
    }



}


void menutour(int tabmains1[],t_data *parametre,int tab[],int *nbAEchanger, int numero) //appel des fonctions lors d'un tour
{
    int choix=0;
  printf(" Joueur %d\n",numero);
    printf(" Voulez-vous placer des tuiles, 1 ou echanger des tuiles, 2 ? "); //propositon au joueur
    scanf("%d",&choix);
    switch(choix)
    {
case 1 :
    {
        placement(tabmains1,&*parametre,tab); // placement de tuiles
        break;

    }
case 2 :
    {
        echange(tabmains1,&*parametre,tab,&*nbAEchanger); //echange de tuiles
        break;
    }
    }
}
void echange (int tabmains1[],t_data *parametre,int tab[],int *nbAEchanger) // permet d'�changer une ou plusieurs tuiles
{
    int i;
    int numerotuile;
    printf(" combien de tuiles souhaitez-vous echanger ? ");

    scanf("%d",&*nbAEchanger);
    for(i=0;i<(*nbAEchanger);i++)
    {
            printf("quel est le numero de la tuile a echanger ? ");
            scanf("%d",&numerotuile);
 tabmains1[numerotuile-1]=pioche(tab);

    }
affichemainj(tabmains1,parametre);

}

void affichemainj (int tabmains1[],t_data *parametre) // affichage de la main d'un joueur

{
int i;
int numero=0;
gotoligcol(38,0);
printf("main du joueur : \n");
    for (i=0; i<6; i++)
    {
numero=tabmains1[i];
        Color(parametre->TabdeTuile[numero].couleur,0);
        printf("%c ",parametre->TabdeTuile[numero].forme);
        Color (15,0);
    }
    printf("\n1 2 3 4 5 6");

}


void mainjou(int tab[],int tabmains1[]) // d�finit la main du joueur 1
{

    int j,i;
    gotoligcol(36,0);

    for(j=0; j<4; j++)
    {




        for (i=0; i<6; i++)
        {
            tabmains1[i]=pioche(tab);

        }

    }
}

int pioche(int tab[]) // pioche une tuile sans redondance
{
    srand(time(NULL));
    int alea=0;

    alea=rand()%(35+1);
    while (tab[alea]==1)
    {
        alea=rand()%(35+1);

    }
    tab[alea]=1;

    return alea;
}





void joueur() //definition des joueurs
{
    int nb=0,i=0;
    char nom1[15],nom2[15],nom3[15],nom4[15];
    printf("Choisir le nombre de joueur\n");
    do
        scanf("%d",&nb);
    while(nb<2||nb>4);
    for (i=1; i<=nb; i++)
    {
        printf("Quel est le nom du joueur %d\n",i);
        switch(i)
        {
        case 1:
        {
            fflush(stdin);
            gets(nom1);

            break;
        }
        case 2:
        {
            fflush(stdin);
            gets(nom2);

            break;
        }
        case 3:
        {
            fflush(stdin);
            gets(nom3);
            break;
        }
        case 4:
        {
            fflush(stdin);
            gets(nom4);
            break;
        }

        }
    }
}

void mode() //menu principal
{
    int mod=0;
    printf("\nDans quel mode voulez vous jouer ?\n1:Degrade\n2:Normal\n3:Regles\n4:Voir les anciens scores\n");
    do
        scanf("%d",&mod);
    while(mod<1||mod>4);
    switch (mod)
    {
    case 1:
        degrade();
        tableau();
        break;
    case 2:
        normal();
        break;
    case 3:
        regle();
        break;
    case 4:
        regle();
        break;
    }
}
void degrade() //appel de la fonction joueur
{
    joueur();



}
void normal()  //appel de la fonction joueur
{
    joueur();
}
void regle() // affiche les r�gles
{
    int a=0;
    printf("Tapez 1 pour revenir en arriere\n\n");
    printf("But du Jeu\nAligner des tuiles ayant des symboles de formes ou de couleurs identiques (mais pas les deux) de facon a realiser des combinaisons rapportant un maximum de points.\n3. Preparation\nLa pioche contient les 108 tuiles qui seront prises au hasard par les joueurs pour completer leur pupitre apres chaque tour.\n Debut de partie\n On peut choisir le mode degrade ou normal (contenu de la pioche different) ainsi que le nombre de joueurs pour la partie, entre 2, 3 et 4.");
    printf("\nOn pioche au hasard six tuiles dans la pioche et celles ci sont place sur le pupitre de chaque joueur, en les maintenant cachees de facon a ce qu aucun autre joueur ne puisse voir les symboles. Ces tuiles forment la main du joueur. Les tuiles restantes forment la pioche.\nLa partie demarre la partie avec le joueur 1 (premier pseudo enregistre). Le pupitre du joueur concerne est revele et la partie est lance ce qui permet au joueur concerne de poser une ou plusieurs tuiles a l endroit de son choix sur la zone de jeu.");
    printf("\nApres la pose des tuiles par le joueur sur la zone de jeu, l ordinateur calcule les points obtenus, affiche les points du coup et le cumul depuis le debut de partie pour ce joueur. L ordinateur preleve aleatoirement de nouvelles tuiles dans la pioche pour avoir a nouveau 6 tuiles sur le pupitre du joueur, tout en masquant de nouveau les 6 tuiles. Le joueur ne voit donc pas les nouvelles tuiles pioches.\nEn appuyant sur la barre d espace, l ordinateur passe au joueur suivant. \n");
    printf("Lorsque tous les joueurs ont joue, l ordinateur demande si on veut arreter la partie ou passer au tour suivant pour l ensemble des joueurs.\nLes joueurs jouent a tour de role dans le sens des aiguilles d une montre (ou le sens croissant et cyclique des numros de joueur).\n5. Deroulement de la partie\nA son tour, un joueur voit son pupitre revele a l ecran. Il peut effectuer l une de ces trois actions :");
    printf("\n1. Ajouter une tuile a une ligne ou a une colonne. L ordinateur masque les tuiles restantes sur le pupitre et pioche une tuile dans la reserve pour en avoir a nouveau six tuiles sur le pupitre du joueur.");
    printf("\n2. Ajouter deux tuiles ou plus a une ligne (ou colonne). Toutes les tuiles jouees a partir de la main du joueur doivent partager une caracteristique, a savoir la couleur ou la forme. Les tuiles du joueur doivent toujours etre posees sur la meme ligne (ou colonne) (il se peut qu elles ne se touchent pas).\nEnsuite, l ordinateur masque les tuiles restantes sur le pupitre et pioche une ou plusieurs tuiles dans la reserve pour en avoir a nouveau six sur le pupitre du joueur.");
    printf("\n3. Echanger tout ou partie des tuiles de sa main contre differentes tuiles de la reserve, et passer son tour (sans jouer de tuile).\nAjouter des tuiles a une ligne (ou colonne)\nChacun a leur tour, les joueurs ajoutent des tuiles a la ligne creee au premier tour sans deborder l espace de jeu. Tous les coups joues doivent etre lies a la ligne existante.");
    printf("\nIl existe six formes et six couleurs. Les joueurs creent des lignes de formes et de couleurs. Deux ou plusieurs tuiles qui se touchent creent une ligne de formes ou une ligne de couleurs. Les tuiles qui sont ajoutees a cette ligne doivent avoir la meme caracteristique que les tuiles qui se trouvent deja sur la ligne. Il peut arriver qu il y ait des places sur la ligne ou aucune tuile ne peut etre ajoutee.");
    printf("\nUne ligne de formes ne peut avoir qu une tuile de chacune des six couleurs. Par exemple, il ne peut y avoir qu un seul carre orange dans une ligne de carres. Une ligne de couleur ne peut avoir qu une tuile de chacune des six formes. Par exemple, il ne peut y avoir qu un rond jaune dans une ligne de jaune.\nEchanger des tuiles\nLorsque c est votre tour, vous pouvez choisir d echanger tout ou partie de vos tuiles au lieu de les ajouter a une ligne.");
    printf("Dans ce cas, vous devez indiquer les tuiles a echanger, puis tirer le meme nombre de tuiles de la reserve (l interface de votre jeu devra permettre cela). Si vous ne pouvez pas ajouter de tuiles a une ligne, vous devez echanger tout ou partie de vos tuiles et passer votre tour.\nCalcul des points\nQuand vous crez une ligne, vous marquez 1 point pour chaque tuile presente dans la ligne. Quand vous ajoutez une tuile a une ligne existante, vous marquez 1 point pour chaque tuile de cette ligne, y compris les tuiles qui se trouvaient au prealable sur cette ligne.");
    printf("\nUne tuile peut rapporter 2 points si elle appartient a deux lignes differentes. Vous marquez 6 points supplementaires chaque fois que vous terminez une ligne de six tuiles. Les six tuiles doivent etre de meme couleur, tout en ayant une forme differente OU de meme forme, tout en ayant une couleur diffrente.\nUne ligne de six tuiles est appelee un Qwirkle (6 points supplementaires).");
    printf("\nLes lignes de plus de six tuiles sont interdites. Le joueur qui termine la partie obtient 6 points supplementaires.\n6. Fin de la partie\nA la fin d�un tour\nApres que tous les joueurs ont pu jouer leur coup, le jeu offre la possibilite de continuer pour le tour suivant ou de terminer la partie. Si les joueurs decident d arreter, le jeu propose d enregistrer le jeu en l tat (toutes les infos necessaires) pour une reprise ulterieure, ou pas.\nQuand la pioche ne contient plus de tuile");
    printf("\nLes joueurs continuent a jouer normalement mais ne tirent plus de tuile a la fin de leur tour. Le premier joueur qui utilise toutes ses tuiles termine la partie et obtient 6 points supplementaires. Si aucun joueur ne peut ajouter de tuile aux lignes existantes et que la reserve (pioche) est vide, le jeu s arrete immediatement a la fin du tour et les 6 points de bonus ne sont pas attribues. Le joueur ayant le plus grand nombre de points remporte la partie, l ordinateur affiche le pseudo et le score de tous les joueurs de la partie.");
    do
        scanf("%d",&a);
    while (a!=1);
    if (a==1)
    {

        mode();
    }
}




void definitiontuiles (t_data *parametre) // definition de la couleur et de la forme de chaque tuile
{
    int i;
    for(i=0; i<6; i++) // initialisation des formes
    {
        parametre->TabdeTuile[i].forme = 'C';


    }
    for(i=6; i<12; i++)
    {
        parametre->TabdeTuile[i].forme = 'T';
    }
    for(i=12; i<18; i++)
    {
        parametre->TabdeTuile[i].forme = 'E';
    }
    for(i=18; i<24; i++)
    {
        parametre->TabdeTuile[i].forme = 'X';
    }
    for(i=24; i<30; i++)
    {
        parametre->TabdeTuile[i].forme = 'L';
    }
    for(i=30; i<36; i++)
    {
        parametre->TabdeTuile[i].forme = 'R';
    }

    for(i=0; i<36; i++) // position 0 donc dans la pioche
    {
        parametre->TabdeTuile[i].position=0;
    }

    for(i=0; i<6; i++)
    {
        parametre->TabdeTuile[6*i].couleur=4; //tuiles rouges


    }
    for(i=0; i<6; i++)
    {
        parametre->TabdeTuile[1+6*i].couleur=5; // tuiles violettes


    }
    for(i=0; i<6; i++)
    {
        parametre->TabdeTuile[2+6*i].couleur=1; // tuiles bleues


    }

    for(i=0; i<6; i++)
    {
        parametre->TabdeTuile[3+6*i].couleur=2; // tuiles vertes


    }
    for(i=0; i<6; i++)
    {
        parametre->TabdeTuile[4+6*i].couleur=8; // tuiles grises


    }

    for(i=0; i<6; i++)
    {
        parametre->TabdeTuile[5+6*i].couleur=14; // tuiles jaunes


    }


} // d�finition des tuiles termin�e

void Color(int couleurDuTexte,int couleurDeFond) // fonction d'affichage de couleurs
{
    HANDLE H=GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(H,couleurDeFond*16+couleurDuTexte);
}




void placement(int tabmains1[],t_data *parametre,int tab[]) //placement des tuiles au cours du jeu
{
    char colonne=0;;
    char ligne=0;
    int numerotuilechevalet=0,numerotuile=0,nombredetuileplacer=0,i;


    printf("Combien de tuiles voulez-vous placer ?\n ");
    scanf("%d",&nombredetuileplacer);
      printf("\n ");

    for(i=0;i<nombredetuileplacer;i++)
    {

gotoligcol(45+2i,0);
    printf("Sur quelle colonne placer une tuile ? ");
    fflush(stdin);
    scanf("%c",&colonne);
    printf("%c\n",colonne);
    printf("Sur quelle ligne placer la tuile ? ");
    fflush(stdin);
    scanf("%c",&ligne);
    printf("%c\n",ligne);

    int lettreAScii=(int)colonne;
    int lettretest=(int)ligne;

    lettreAScii=5+(lettreAScii-97)*4; //position de la colonne pour le curseur
    lettretest=4+(lettretest-97)*2; //position de la ligne

    printf("quel est le numero de la tuile que vous souhaitez inserer ? ");
    scanf("%d",&numerotuilechevalet);
    numerotuile=tabmains1[numerotuilechevalet-1];

    gotoligcol(lettretest,lettreAScii);  // place le curseur ligne, colonne � l'�cran
    Color(parametre->TabdeTuile[numerotuile].couleur,0);// d�finit la couleur demand�e
        printf("%c",parametre->TabdeTuile[numerotuile].forme); // et �crit la tuile � cet endroit



    Color(15,0);
    tabmains1[numerotuilechevalet-1]=pioche(tab);
    }
    gotoligcol(40,1);
}

void gotoligcol( int lig, int col ) // placement du curseur
{
// ressources
    COORD mycoord;

    mycoord.X = col;
    mycoord.Y = lig;
    SetConsoleCursorPosition( GetStdHandle( STD_OUTPUT_HANDLE ), mycoord );
}
void tableau() // affichage de la grille de jeu
{

    gotoligcol(0,0);
    printf(" ");

    printf ("\n");
    printf ("   | a | b | c | d | e | f | g | h | i | j | k | l | m | n | o | p | q | r | s | t | u | v | w | x | y | z |\n");
    printf("___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|  \n");
    printf("   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |     \n");
    printf (" a |___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|   \n");
    printf("   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |     \n");
    printf (" b |___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|   \n");
    printf("   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |     \n");
    printf (" c |___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|   \n");
    printf("   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |     \n");
    printf (" d |___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|   \n");
    printf("   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |     \n");
    printf (" e |___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|   \n");
    printf("   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |     \n");
    printf (" f |___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|   \n");
    printf("   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |     \n");
    printf (" g |___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|   \n");
    printf("   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |     \n");
    printf (" h |___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|   \n");
    printf("   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |     \n");
    printf (" i |___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|   \n");
    printf("   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |     \n");
    printf (" j |___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|   \n");
    printf("   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |     \n");
    printf (" k |___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|   \n");
    printf("   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |     \n");
    printf (" l |___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|   \n");

}












