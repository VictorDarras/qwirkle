#ifndef FONCTIONS99_H_INCLUDED
#define FONCTIONS99_H_INCLUDED
// prototypes des fonctions
typedef struct mainjoueur
{
int    tabnumero[6];
}t_mainj;

typedef struct jou
{
    t_mainj Tabdemains[4];
}t_jou;

typedef struct tuile
{
    char forme;
    int couleur;
    int position;
} t_tuile;

typedef struct Data
{
    t_tuile TabdeTuile[36];

} t_data;
void Color(int couleurDuTexte,int couleurDeFond);
void menutour(int tabmains1[],t_data *parametre,int tab[],int *nbAEchanger,int numero);

void echange (int tabmains1[],t_data *parametre,int tab[],int *nbAEchanger);


void degrade();
void normal();
void regle();
void joueur();
void mainjou(int tab[],int tabmains1[]);
void affichemainj (int tabmains1[],t_data *parametre);



void mode();


int pioche(int tab[]);


void tableau();

void placement(int tabmains1[],t_data *parametre,int tab[]);

void lecturescores ();
void definitiontuiles (t_data *parametre);
void gotoligcol( int lig, int col );
#endif // FONCTIONS99_H_INCLUDED
